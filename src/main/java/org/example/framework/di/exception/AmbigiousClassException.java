package org.example.framework.di.exception;

public class AmbigiousClassException extends RuntimeException {
    public AmbigiousClassException() {
    }

    public AmbigiousClassException(final String message) {
        super(message);
    }

    public AmbigiousClassException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AmbigiousClassException(final Throwable cause) {
        super(cause);
    }

    public AmbigiousClassException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
