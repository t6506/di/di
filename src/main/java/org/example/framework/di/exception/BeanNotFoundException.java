package org.example.framework.di.exception;

public class BeanNotFoundException extends RuntimeException {
    public BeanNotFoundException() {
    }

    public BeanNotFoundException(final String message) {
        super(message);
    }

    public BeanNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public BeanNotFoundException(final Throwable cause) {
        super(cause);
    }

    public BeanNotFoundException(final String message, final Throwable cause, final boolean enableSuppression,
                                 final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
