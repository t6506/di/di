package org.example.framework.di.processor;


public interface BeanPostProcessor {
    boolean canProcessed(Class<?> clazz);
    Object process(Object object, Class<?> clazz);
}
